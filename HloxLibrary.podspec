#
# Be sure to run `pod lib lint HloxLibrary.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HloxLibrary'
  s.version          = '0.1.0'
  s.summary          = 'Take a look at this wonderful library.'
  s.description      = 'This is the most coolest Library you have never used in your whole life'
  s.homepage         = 'https://bitbucket.org/LehlohonoloIsaac/hloxlibrary/src/master/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Lehlohonolo Isaac' => 'lehlohonoloisaac25@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/LehlohonoloIsaac/hloxlibrary/src/master/', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'HloxLibrary/Classes/**/*'
  s.frameworks = 'UIKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
