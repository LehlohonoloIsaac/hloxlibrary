//
//  PrintableName.swift
//  HloxLibrary
//
//  Created by Lehlohonolo Mbele on 2018/04/12.
//

import Foundation

public class PrintableName {
    
    var name: String
    
    init(_ name: String) {
        self.name = name
    }
    
    func printName() {
        print("my name is \(name)")
    }
}
