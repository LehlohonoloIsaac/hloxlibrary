# HloxLibrary

[![CI Status](http://img.shields.io/travis/lehlohonoloisaac25@gmail.com/HloxLibrary.svg?style=flat)](https://travis-ci.org/lehlohonoloisaac25@gmail.com/HloxLibrary)
[![Version](https://img.shields.io/cocoapods/v/HloxLibrary.svg?style=flat)](http://cocoapods.org/pods/HloxLibrary)
[![License](https://img.shields.io/cocoapods/l/HloxLibrary.svg?style=flat)](http://cocoapods.org/pods/HloxLibrary)
[![Platform](https://img.shields.io/cocoapods/p/HloxLibrary.svg?style=flat)](http://cocoapods.org/pods/HloxLibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HloxLibrary is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'HloxLibrary'
```

## Author

lehlohonoloisaac25@gmail.com, lehlohonoloisaac25@gmail.com

## License

HloxLibrary is available under the MIT license. See the LICENSE file for more info.
